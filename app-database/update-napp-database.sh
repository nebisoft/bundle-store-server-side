#!/usr/bin/bash

rm ./--database--.txt
for f in $(ls ./*.napp)
do
	echo $f
	name=$(echo $f | sed "s/\.napp//g" | sed "s/.\///g" | sed "s/_/ /g")
	nameorig=$(echo $f | sed "s/\.napp//g" | sed "s/.\///g")
	categories=$(bash ./$f --lsm | grep "Categories=" | sed "s/Categories=//g" | sed "s/;.*//g")
	keywords=$(bash ./$f --lsm | grep "Keywords=" | sed "s/Keywords=//g")
	echo "$name|$categories|$keywords" >> ./--database--.txt
	echo "# $(bash ./$f --lsm | grep " Name=" | sed "s/ Name=//g")
$(bash ./$f --lsm | grep "Comment=" | sed "s/Comment=//g")" > ./$nameorig.md
done
chmod 755 ./*
