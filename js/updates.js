function formatItem(item, ver) {
    return '<div style="display: flex;"><img id="updaterIcon" src="app-database/' + item[0].replaceAll(" ", "_") + '.png" /><p id="updaterAppName">'+ item[0].replaceAll("_", " ") + '</p></div> <td><a href="app-database/' + item[0].replaceAll(" ", "_") + '.napp" style="float: right;" class="button-accent">Update to ' + ver + '</a></td>';   
}

function getUpdates(appAndVersionOrig) {
    var appAndVersion = appAndVersionOrig;
    var AppName = appAndVersion.split("|")[0];
    var AppVersion = appAndVersion.split("|")[1];
    try {
        $.get("../app-database/" + AppName + ".ninf").then(
            function (data) {
                var lines = data.match(/^.*([\n\r]+|$)/gm);
                for (let index = 0; index < lines.length; index++) {
                    const element = lines[index];
                    if(element.split("=")[0] == "Version") {
                        var DatabaseAppVer = element.split("=")[1].replaceAll('"', "").replaceAll("\n", "");
                        if (DatabaseAppVer == AppVersion) {
                            console.log(AppName + " is in latest version - Version " + AppVersion)
                        }
                        else {
                            console.log(AppName + " needs to be updated - Version " + AppVersion + " >> " + DatabaseAppVer)
                            $('<tr>', { html: formatItem(appAndVersion.split("|"), DatabaseAppVer) }).appendTo($("#updater"));
                        }
                    }
                }
            }
        );
    } catch (error) {
        console.log(AppName + ": " + error);
    }
}

getParameter = (key) => {
  
    // Address of the current window
    address = window.location.search
  
    // Returns a URLSearchParams object instance
    parameterList = new URLSearchParams(address)
  
    // Returning the respected value associated
    // with the provided key
    return parameterList.get(key)
}

var ua = navigator.userAgent;
if (ua.startsWith("Bundle Store - NebiOS ")) {
    var verNum = parseFloat(ua.replace("Bundle Store - NebiOS ", ""));
}
else { var verNum = 2.0; }
var sysAppsStr = "NebiOS_Update|" + verNum.toString() + ";" + getParameter("list")
var sysAppsList = sysAppsStr.split(";");
sysAppsList.pop();
for (let index = 0; index < sysAppsList.length; index++) {
    const element = sysAppsList[index];
    getUpdates(element);
}
