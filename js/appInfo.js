function url_content(url){
    return $.get(url);
}

function checkImage() {
    var request = new XMLHttpRequest();
    request.open("GET", "https://bundlestoreclient.nebisoftware.com/app-database/" + getParameter("name") + ".Screenshot.png", true);
    request.send();
    request.onload = function() {
      status = request.status;
      if (request.status == 404) //if(statusText == OK)
      {
        return false;
      } else {
            document.getElementById("appScreenshot").src = "../app-database/" + getParameter("name") + ".Screenshot.png"
      }
    }
  }

getParameter = (key) => {
  
    // Address of the current window
    address = window.location.search
  
    // Returns a URLSearchParams object instance
    parameterList = new URLSearchParams(address)
  
    // Returning the respected value associated
    // with the provided key
    return parameterList.get(key)
}

document.getElementById("appIcon").src = "../app-database/" + getParameter("name") + ".png"
checkImage();
document.getElementById("appDownload").href = "../app-database/" + getParameter("name") + ".napp"

url_content("../app-database/" + getParameter("name") + ".ninf").then(function(data){ 
    $(function() {
        var lines = data.match(/^.*([\n\r]+|$)/gm);
        for(var i = 0;i < lines.length;i++){
            if (lines[i].startsWith(" Name=")) {
                var AppName = lines[i].replace(" Name=", "");
                if (AppName.startsWith("Microsoft Office")) {
                    $("#somethingBetterThanThis").show();
                    $("#spywareAppWarning").show();
                }
                if (AppName.startsWith("iTunes") || 
                    AppName.startsWith("Discord") ||
                    AppName.startsWith("Spotify")) {
                        $("#spywareAppWarning").show();
                }
                $("#appName").text(AppName);
            }
            if (lines[i].startsWith("Version=")) {
                var AppVersion = lines[i].replace("Version=", "").replaceAll("'", "").replaceAll('"', "");
                $("#appVer").text(AppVersion);
            }
            if (lines[i].startsWith("StoreDescription=")) {
                var AppDesc = lines[i].replace('StoreDescription=', "").replaceAll("'", "").replaceAll('"', "").replaceAll("\\n", "<br>");
                $("#appDesc").html(AppDesc);
            }
            if (lines[i].startsWith("Categories=")) {
                var AppCat = lines[i].replace('Categories=', "").replaceAll("'", "").replaceAll('"', "").replaceAll("\\n", "<br>");
                $("#appCategory").text(AppCat.split(";")[0]);
            }
            if (lines[i].startsWith("Author=")) {
                var AppAuth = lines[i].replace('Author=', "").replaceAll("'", "").replaceAll('"', "").replaceAll("\\n", "<br>");
                $("#appMaker").text("by " + AppAuth);
            }
            if (lines[i].startsWith("Maintainer=")) {
                var AppAuth = lines[i].replace('Maintainer=', "").replaceAll("'", "").replaceAll('"', "").replaceAll("\\n", "<br>");
                $("#appPublisher").text(AppAuth);
            }
            if (lines[i].startsWith("Size=")) {
                var AppAuth = lines[i].replace('Size=', "").replaceAll("'", "").replaceAll('"', "").replaceAll("\\n", "<br>");
                $("#appSize").text(AppAuth);
            }
            if (lines[i].startsWith("License=")) {
                var AppAuth = lines[i].replace('License=', "").replaceAll("'", "").replaceAll('"', "").replaceAll("\\n", "<br>");
                if (AppAuth == "\n") {
                    $("#appLicense").text("Proprietary");
                }
                else
                {
                    $("#appLicense").text(AppAuth);
                }
            }
        }
    });
});