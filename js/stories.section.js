function url_content(url){
    return $.get(url);
}

function getAverageRGB(imgEl) {

    var blockSize = 5, // only visit every 5 pixels
        defaultRGB = {r:53,g:53,b:53}, // for non-supporting envs
        canvas = document.createElement('canvas'),
        context = canvas.getContext && canvas.getContext('2d'),
        data, width, height,
        i = -4,
        length,
        rgb = {r:0,g:0,b:0},
        count = 0;

    if (!context) {
        return defaultRGB;
    }

    height = canvas.height = imgEl.naturalHeight || imgEl.offsetHeight || imgEl.height;
    width = canvas.width = imgEl.naturalWidth || imgEl.offsetWidth || imgEl.width;

    context.drawImage(imgEl, 0, 0);

    try {
        data = context.getImageData(0, 0, width, height);
    } catch(e) {
        /* security error, img on diff domain */
        return defaultRGB;
    }

    length = data.data.length;

    while ( (i += blockSize * 4) < length ) {
        ++count;
        rgb.r += data.data[i];
        rgb.g += data.data[i+1];
        rgb.b += data.data[i+2];
    }

    // ~~ used to floor values
    rgb.r = ~~(rgb.r/count);
    rgb.g = ~~(rgb.g/count);
    rgb.b = ~~(rgb.b/count);

    return rgb;

}

function gradient() {
    document.getElementById("featured-app-big-bg").hidden = true;
    setTimeout(function() {
        document.getElementById("featured-app-big").style.height = "360px";
        var rgb = getAverageRGB(document.getElementById("featured-app-big-icon"));
        document.getElementById("featured-app-big").style.background = 'rgb('+rgb.r+','+rgb.g+','+rgb.b+')';
        document.getElementById("featured-app-big-icon").hidden = false;
        document.getElementById("featured-app-big-content").style.display = "flex";
    }, 250);
}

function shuffle(array) {
    let currentIndex = array.length,  randomIndex;
  
    // While there remain elements to shuffle.
    while (currentIndex != 0) {
  
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
  }
  $( document ).ready(function() {
url_content("../app-database/--database--.txt").then(function(data){ 
    $(function() {
        var lines = data.match(/^.*([\n\r]+|$)/gm);
        lines.pop();

        var randomAppAsInt = Math.floor(Math.random() * lines.length);
        var randomApp = lines[randomAppAsInt];
        var randomNameCatKey = randomApp.split("|");
        randomNameCatKey[0] = randomNameCatKey[0].replaceAll(" ", "_");
        document.getElementById("featured-app-big-content").outerHTML = "<zero-md id='featured-app-big-content' style='display: none;' src='./app-database/" + randomNameCatKey[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: right;} .markdown-body {position:absolute; width: 65%; top: 50%; left: 50%; transform: translate(-50%, -50%);}</style></template></zero-md>";
        document.getElementById("featured-app-big-icon").src = "./app-database/" + randomNameCatKey[0] + ".png";
        document.getElementById("featured-app-big").href = "appinfo.html?name=" + randomNameCatKey[0];
        document.getElementById("featured-app-big-bg").src = "./app-database/" + randomNameCatKey[0] + ".Featured.png";
        lines = lines.filter(function(item) {
            return item !== randomApp;
        });

        var Development = [];
        var Education = [];
        var Games = [];
        var Graphics = [];
        var Multimedia = [];
        var Network = [];
        var Office = [];
        var Settings = [];
        var System = [];
        var Utility = [];

        for (let index = 0; index < lines.length; index++) {
            const element = lines[index];
            var nameCatKeyOrig = element.split("|")
            if (nameCatKeyOrig[1] == "Development") { Development.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
            if (nameCatKeyOrig[1] == "Education") { Education.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
            if (nameCatKeyOrig[1] == "Games") { Games.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
            if (nameCatKeyOrig[1] == "Graphics") { Graphics.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
            if (nameCatKeyOrig[1] == "Multimedia") { Multimedia.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
            if (nameCatKeyOrig[1] == "Network") { Network.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
            if (nameCatKeyOrig[1] == "Office") { Office.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
            if (nameCatKeyOrig[1] == "Settings") { Settings.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
            if (nameCatKeyOrig[1] == "System") { System.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
            if (nameCatKeyOrig[1] == "Utility") { Utility.push(nameCatKeyOrig[0].replaceAll(" ", "_")) }
        }

         Development = shuffle(Development);
         Education = shuffle(Education);
         Games = shuffle(Games);
         Graphics = shuffle(Graphics);
         Multimedia = shuffle(Multimedia);
         Network = shuffle(Network);
         Office = shuffle(Office);
         Settings = shuffle(Settings);
         System = shuffle(System);
         Utility = shuffle(Utility);

         Development = Development.slice(0,7);
         Education = Education.slice(0,7);
         Games = Games.slice(0,7);
         Graphics = Graphics.slice(0,7);
         Multimedia = Multimedia.slice(0,7);
         Network = Network.slice(0,7);
         Office = Office.slice(0,7);
         Settings = Settings.slice(0,7);
         System = System.slice(0,7);
         Utility = Utility.slice(0,7);
        if (Development.length != 0) {
         $(".Development").find("#featured-app-middle-icon").attr("src", "./app-database/" + Development[0] + ".png");
         setTimeout(function() {
            var Development_Featured_RGB = getAverageRGB(document.querySelector(".Development").querySelector("#featured-app-middle-icon"));
            $(".Development").find("#container").parent().attr("style", "background: rgb(" + Development_Featured_RGB.r + ", " + Development_Featured_RGB.g + ", " + Development_Featured_RGB.b + ");");
            $(".Development").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + Development[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("Development")[0].href = "appinfo.html?name=" + Development[0];
         }, 250);} else {$(".Development").parent().attr("style","display:none; margin: 0 !important;");}

         if (Education.length > 0) {
         $(".Education").find("#featured-app-middle-icon").attr("src", "./app-database/" + Education[0] + ".png");
         setTimeout(function() {
            var Education_Featured_RGB = getAverageRGB(document.querySelector(".Education").querySelector("#featured-app-middle-icon"));
            $(".Education").find("#container").parent().attr("style", "background: rgb(" + Education_Featured_RGB.r + ", " + Education_Featured_RGB.g + ", " + Education_Featured_RGB.b + ");");
            $(".Education").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + Education[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("Education")[0].href = "appinfo.html?name=" + Education[0];
        }, 250);} else {$(".Education").parent().attr("style","display:none; margin: 0 !important;");}

        if (Games.length > 0) {
         $(".Games").find("#featured-app-middle-icon").attr("src", "./app-database/" + Games[0] + ".png");
         setTimeout(function() {
            var Games_Featured_RGB = getAverageRGB(document.querySelector(".Games").querySelector("#featured-app-middle-icon"));
            $(".Games").find("#container").parent().attr("style", "background: rgb(" + Games_Featured_RGB.r + ", " + Games_Featured_RGB.g + ", " + Games_Featured_RGB.b + ");");
            $(".Games").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + Games[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("Games")[0].href = "appinfo.html?name=" + Games[0];
        }, 250);} else {$(".Games").parent().attr("style","display:none; margin: 0 !important;");}

        if (Graphics.length > 0) {
         $(".Graphics").find("#featured-app-middle-icon").attr("src", "./app-database/" + Graphics[0] + ".png");
         setTimeout(function() {
            var Graphics_Featured_RGB = getAverageRGB(document.querySelector(".Graphics").querySelector("#featured-app-middle-icon"));
            $(".Graphics").find("#container").parent().attr("style", "background: rgb(" + Graphics_Featured_RGB.r + ", " + Graphics_Featured_RGB.g + ", " + Graphics_Featured_RGB.b + ");");
            $(".Graphics").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + Graphics[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("Graphics")[0].href = "appinfo.html?name=" + Graphics[0];
        }, 250);} else {$(".Graphics").parent().attr("style","display:none; margin: 0 !important;");}

        if (Multimedia.length > 0) {
         $(".Multimedia").find("#featured-app-middle-icon").attr("src", "./app-database/" + Multimedia[0] + ".png");
         setTimeout(function() {
            var Multimedia_Featured_RGB = getAverageRGB(document.querySelector(".Multimedia").querySelector("#featured-app-middle-icon"));
            $(".Multimedia").find("#container").parent().attr("style", "background: rgb(" + Multimedia_Featured_RGB.r + ", " + Multimedia_Featured_RGB.g + ", " + Multimedia_Featured_RGB.b + ");");
            $(".Multimedia").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + Multimedia[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("Multimedia")[0].href = "appinfo.html?name=" + Multimedia[0];
        }, 250);} else {$(".Multimedia").parent().attr("style","display:none; margin: 0 !important;");}

        if (Network.length > 0) {
         $(".Network").find("#featured-app-middle-icon").attr("src", "./app-database/" + Network[0] + ".png");
         setTimeout(function() {
            var Network_Featured_RGB = getAverageRGB(document.querySelector(".Network").querySelector("#featured-app-middle-icon"));
            $(".Network").find("#container").parent().attr("style", "background: rgb(" + Network_Featured_RGB.r + ", " + Network_Featured_RGB.g + ", " + Network_Featured_RGB.b + ");");
            $(".Network").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + Network[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("Network")[0].href = "appinfo.html?name=" + Network[0];
        }, 250);} else {$(".Network").parent().attr("style","display:none; margin: 0 !important;");}

        if (Office.length > 0) {
         $(".Office").find("#featured-app-middle-icon").attr("src", "./app-database/" + Office[0] + ".png");
         setTimeout(function() {
            var Office_Featured_RGB = getAverageRGB(document.querySelector(".Office").querySelector("#featured-app-middle-icon"));
            $(".Office").find("#container").parent().attr("style", "background: rgb(" + Office_Featured_RGB.r + ", " + Office_Featured_RGB.g + ", " + Office_Featured_RGB.b + ");");
            $(".Office").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + Office[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("Office")[0].href = "appinfo.html?name=" + Office[0];
        }, 250);} else {$(".Office").parent().attr("style","display:none; margin: 0 !important;");}

        if (Settings.length > 0) {
         $(".Settings").find("#featured-app-middle-icon").attr("src", "./app-database/" + Settings[0] + ".png");
         setTimeout(function() {
            var Settings_Featured_RGB = getAverageRGB(document.querySelector(".Settings").querySelector("#featured-app-middle-icon"));
            $(".Settings").find("#container").parent().attr("style", "background: rgb(" + Settings_Featured_RGB.r + ", " + Settings_Featured_RGB.g + ", " + Settings_Featured_RGB.b + ");");
            $(".Settings").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + Settings[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("Settings")[0].href = "appinfo.html?name=" + Settings[0];
        }, 250);} else {$(".Settings").parent().attr("style","display:none; margin: 0 !important;");}

        if (System.length > 0) {
         $(".System").find("#featured-app-middle-icon").attr("src", "./app-database/" + System[0] + ".png");
         setTimeout(function() {
            var System_Featured_RGB = getAverageRGB(document.querySelector(".System").querySelector("#featured-app-middle-icon"));
            $(".System").find("#container").parent().attr("style", "background: rgb(" + System_Featured_RGB.r + ", " + System_Featured_RGB.g + ", " + System_Featured_RGB.b + ");");
            $(".System").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + System[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("System")[0].href = "appinfo.html?name=" + System[0];
        }, 250);} else {$(".System").parent().attr("style","display:none; margin: 0 !important;");}

        if (Utility.length > 0) {
         $(".Utility").find("#featured-app-middle-icon").attr("src", "./app-database/" + Utility[0] + ".png");
         setTimeout(function() {
            var Utility_Featured_RGB = getAverageRGB(document.querySelector(".Utility").querySelector("#featured-app-middle-icon"));
            $(".Utility").find("#container").parent().attr("style", "background: rgb(" + Utility_Featured_RGB.r + ", " + Utility_Featured_RGB.g + ", " + Utility_Featured_RGB.b + ");");
            $(".Utility").find("#featured-app-middle-content").html("<zero-md src='./app-database/" + Utility[0] + ".md'><template><style>* {font-family: Karla; color: white; text-align: left;} h1 { margin-top: 0; } .markdown-body {position: absolute; height:100%;}</style></template></zero-md>");
            document.getElementsByClassName("Utility")[0].href = "appinfo.html?name=" + Utility[0];
        }, 250);} else {$(".Utility").parent().attr("style","display:none; margin: 0 !important;");}

        for (let index = 1; index < Development.length; index++) {
            const element = Development[index];
            var appBox = document.querySelector(".Development").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        for (let index = 1; index < Education.length; index++) {
            const element = Education[index];
            var appBox = document.querySelector(".Education").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        for (let index = 1; index < Games.length; index++) {
            const element = Games[index];
            var appBox = document.querySelector(".Games").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        for (let index = 1; index < Graphics.length; index++) {
            const element = Graphics[index];
            var appBox = document.querySelector(".Graphics").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        for (let index = 1; index < Multimedia.length; index++) {
            const element = Multimedia[index];
            var appBox = document.querySelector(".Multimedia").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        for (let index = 1; index < Network.length; index++) {
            const element = Network[index];
            var appBox = document.querySelector(".Network").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        for (let index = 1; index < Office.length; index++) {
            const element = Office[index];
            var appBox = document.querySelector(".Office").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        for (let index = 1; index < Settings.length; index++) {
            const element = Settings[index];
            var appBox = document.querySelector(".Settings").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        for (let index = 1; index < System.length; index++) {
            const element = System[index];
            var appBox = document.querySelector(".System").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        for (let index = 1; index < Utility.length; index++) {
            const element = Utility[index];
            var appBox = document.querySelector(".Utility").parentElement.querySelector("#featured-app");
            appBox.innerHTML = appBox.innerHTML + '<a href="appinfo.html?name=' + element + '" id="featured-app-small"><img id="featured-app-small-icon" src="' + "./app-database/" + element + ".png" + '" />' + element.replaceAll("_", " ") + '</a>'
        }
        
    });
});
});
