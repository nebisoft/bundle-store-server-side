function url_content(url){
    document.getElementById("searcherBegin").style.display = "none";
    return $.get(url);
}

function getdesc(descdata) {
    var desc = descdata;
    return desc[0];
}

function formatItem(item) {
        return '<div style="display: flex;"><img id="searcherIcon" src="app-database/' + item.AppName.replaceAll(" ", "_") + '.png" /><a href="appinfo.html?name=' + item.AppName + '" id="searcherAppName">' + item.AppName.replaceAll("_", " ") + '</a></div> <td id="searcherCategory"> ' + item.Category + ' </td><td id="searcherKeywords">' + item.Keywords +'</td>';   
}

function search(val) {
    var value = val.toLowerCase();
    $("#searcher tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });

document.getElementById("searcherBegin").style.display = "none";
}

url_content("../app-database/--database--.txt").then(function(data){ 
    $(function() {
        var lines = data.match(/^.*([\n\r]+|$)/gm);
        lines.pop()
        for (let index = 0; index < lines.length; index++) {
            const element = lines[index];
            var nameCatKeyOrig = element.split("|")
                var datas = [
                    {"AppName":nameCatKeyOrig[0],"Category":nameCatKeyOrig[1],"Keywords":nameCatKeyOrig[2].replaceAll(";",", ")}
                ];
            $.each(datas, function (key, item) {
                $('<tr>', { html: formatItem(item) }).appendTo($("#searcher"));
            });
        }
        getParameter = (key) => {
  
            // Address of the current window
            address = window.location.search
          
            // Returns a URLSearchParams object instance
            parameterList = new URLSearchParams(address)
          
            // Returning the respected value associated
            // with the provided key
            return parameterList.get(key)
        }
    
        search(getParameter("search"))
    });
});
