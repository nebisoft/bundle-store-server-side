# Bundle Store server-side source code
Source code for Bundle Store server side software. IT IS NOT READY FOR CREATING A BUNDLE STORE REPOSITORY SINCE INCOMPLETE!
## Features
* Fetchs apps from database (app-databse/--database--.txt). It can updatable via app-database/update-napp-database.sh script (if you want, you can connect into a cron job).
* Loads random apps when every loading the index.html file.
* Search via JQuery (only works in Bundle Store Client - NebiOS).
## To-Do
* Improve browser exprience with in-site searchbar.
* Implement custom repositories feature.
* Categories.
## Requirements
* A server/shared hosting running Ubuntu, NebiOS, CentOS or any Linux with bash. WINDOWS (WITH WSL OR NOT) OR MAC SERVERS DOESN'T WORK.
* Bundled apps for your store repository.
* A human with Linux/Unix knowledge.
# Installation
* `git clone` or download zip and extract it.
* Upload your apps and update the database (see below).
* Go to your Bundle Store repository from browser.
* Enjoy.
## Uploading your apps
* NOTE 1: App file name can't use ", (space), \, / & unicode characters.
* NOTE 2: If you want, you can upload a screenshot (AppName.Screenshot.png) & featured graphic (AppName.Featured.png)
* If you're using Cloudflare, open "Development Mode" while making this progress or "Purge Cache" at the ending.
* Upload your app (YourApp.ninf, YourApp.napp & YourApp.png icon file - Files must need same name.)
* To update server-side app database, run `cd app-database && bash ./update-napp-databse.sh` .
## Using your repository in Bundle Store client
* Switch to NebiOS (Since Bundle Store client not natively available in Win32 and Darwin platforms).
* In Bundle Store home page, go to end of the page and click to "Custom app repositories...".
* Add your repository's domain (as in this format: https://YourRepo.com).
* Click save and start searching your uploaded apps.